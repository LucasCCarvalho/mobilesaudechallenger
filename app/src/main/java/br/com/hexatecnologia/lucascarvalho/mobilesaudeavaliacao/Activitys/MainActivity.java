package br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Activitys;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Adapters.GridViewAdapter;
import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Models.Results;
import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.R;
import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Services.TheMovieService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener {

    Spinner spnTypeFilms;
    String typeFilms[] = {"Mais Populares", "Melhor Classificação"};
    ArrayAdapter <String> adapterSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createVisual();

        spnTypeFilms = findViewById(R.id.spnTypeFilms);
        adapterSpinner = new ArrayAdapter<String>(this, R.layout.spinner_text, typeFilms);
        adapterSpinner.setDropDownViewResource(R.layout.spinner_item);
        spnTypeFilms.setAdapter(adapterSpinner);


        spnTypeFilms.setOnItemSelectedListener(this);


    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
        Log.d("Selected", String.valueOf(pos));

        if(pos == 0) {
            CallRetroficService("0");
        } else if (pos == 1) {
            CallRetroficService("1");

        }

        Toast.makeText(parent.getContext(),
                "Você está na categoria: " + parent.getItemAtPosition(pos).toString(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void CallRetroficService(String TypeCall) {
        final GridView gridView = findViewById(R.id.gridViewFilms);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TheMovieService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TheMovieService service = retrofit.create(TheMovieService.class);

        if (TypeCall == "0") {
            Call<Results> requestFilmsPopular = service.listFilmsPopular();
            requestFilmsPopular.enqueue(new Callback<Results>() {
                @Override
                public void onResponse(Call<Results> call, Response<Results> response) {
                    if (!response.isSuccessful()) {
                        Log.e("ErrorApp", "Error: " + response.code());
                    } else {
                        Results results = response.body();
                        GridViewAdapter adapter = new GridViewAdapter(results.results, getApplicationContext());
                        gridView.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<Results> call, Throwable t) {
                    Log.e("ErrorApp", "Error: " + t.getMessage());
                }
            });
        } else if (TypeCall == "1") {
            Call<Results> requestFilmsPopular = service.listFilmsTopRated();
            requestFilmsPopular.enqueue(new Callback<Results>() {
                @Override
                public void onResponse(Call<Results> call, Response<Results> response) {
                    if (!response.isSuccessful()) {
                        Log.e("ErrorApp", "Error: " + response.code());
                    } else {
                        Results results = response.body();
                        GridViewAdapter adapter = new GridViewAdapter(results.results, getApplicationContext());
                        gridView.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<Results> call, Throwable t) {
                    Log.e("ErrorApp", "Error: " + t.getMessage());
                }
            });
        }
    }

    public void createVisual(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorTitle));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}