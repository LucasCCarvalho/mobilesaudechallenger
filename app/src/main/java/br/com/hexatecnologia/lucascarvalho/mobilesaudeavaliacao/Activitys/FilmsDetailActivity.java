package br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Activitys;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.R;

public class FilmsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_films_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView txvTitle = findViewById(R.id.txvTitle);
        TextView txvOriginalTitle = findViewById(R.id.txvOriginalTitle);
        TextView txvRealeseData = findViewById(R.id.txvReleaseDate);
        TextView txvOverView = findViewById(R.id.txvOverview);
        TextView txvVote = findViewById(R.id.txvVoteAverage);
        ImageView imvPostPath = findViewById(R.id.imvPostarPath);

        Bundle extras = getIntent().getExtras();
        txvTitle.setText(extras.getString("title"));
        txvOriginalTitle.setText(extras.getString("titleOriginal"));
        String[] dataSplit = extras.getString("realeseData").split("-");
        txvRealeseData.setText(dataSplit[2]+"/"+dataSplit[1]+"/"+dataSplit[0]);
        txvOverView.setText(extras.getString("overview"));
        txvVote.setText(extras.getString("vote"));
        Picasso.get().load("http://image.tmdb.org/t/p/w185"+extras.getString("image")).into(imvPostPath);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.actionVote) {
            AlertDialog.Builder builder = new AlertDialog.Builder(FilmsDetailActivity.this);
            View view = LayoutInflater.from(FilmsDetailActivity.this).inflate(R.layout.custom_alert_dialog, null);


            builder.setPositiveButton("Avaliar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(FilmsDetailActivity.this, "Obrigado por votar!", Toast.LENGTH_SHORT).show();
                }
            });

            builder.setNegativeButton("Desistir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(FilmsDetailActivity.this, "Até logo!", Toast.LENGTH_SHORT).show();
                }
            });

            builder.setView(view);
            builder.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
