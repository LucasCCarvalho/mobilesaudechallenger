package br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Activitys.FilmsDetailActivity;
import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Activitys.MainActivity;
import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Models.Films;
import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.R;

public class GridViewAdapter extends BaseAdapter {

    List<Films> lstFilms;
    Context mContext;
    private static LayoutInflater inflater=null;

    public GridViewAdapter(List<Films> lstFilms, Context mContext) {
        this.lstFilms = lstFilms;
        this.mContext = mContext;
        inflater = ( LayoutInflater )mContext.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return lstFilms.size();
    }

    @Override
    public Object getItem(int position) {
        return lstFilms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView filmsTitles;
        ImageView filmsImages;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.gridview_item, null);
        holder.filmsTitles = rowView.findViewById(R.id.filmsTitles);
        holder.filmsImages = rowView.findViewById(R.id.filmsImages);

        Picasso.get().load("http://image.tmdb.org/t/p/w185"+lstFilms.get(position).poster_path).into(holder.filmsImages);
        holder.filmsTitles.setText(lstFilms.get(position).title);

        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent call = new Intent(mContext, FilmsDetailActivity.class);
                List<String> test;
                Bundle extras = new Bundle();
                extras.putString("title", lstFilms.get(position).title);
                extras.putString("titleOriginal", lstFilms.get(position).original_title);
                extras.putString("realeseData",  lstFilms.get(position).release_date);
                extras.putString("overview", lstFilms.get(position).overview);
                extras.putString("vote", lstFilms.get(position).vote_average);
                extras.putString("image", lstFilms.get(position).poster_path);
                call.putExtras(extras);
                call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                mContext.startActivity(call);

            }
        });

        return rowView;
    }
}
