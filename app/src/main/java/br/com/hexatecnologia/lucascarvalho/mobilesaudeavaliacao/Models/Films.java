package br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Models;

public class Films {

    public String id;
    public String title;
    public String poster_path;
    public String vote_average;
    public String overview;
    public String release_date;
    public String original_title;

}
