package br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Services;

import br.com.hexatecnologia.lucascarvalho.mobilesaudeavaliacao.Models.Results;
import retrofit2.Call;
import retrofit2.http.GET;

public interface TheMovieService {

    public static final String BASE_URL = "https://api.themoviedb.org/3/";

    @GET("movie/top_rated?api_key=bcd6c2c21e1e75b74d1e2d38ddc9fe0b&language=en-US&page=1")
    Call<Results> listFilmsTopRated();

    @GET("movie/popular?api_key=bcd6c2c21e1e75b74d1e2d38ddc9fe0b&language=en-US&page=1")
    Call<Results> listFilmsPopular();

}
